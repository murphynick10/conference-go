import requests
from .keys import PEXELS_KEY, OPEN_WEATHER_API_KEY
import json


def getCityPhoto(city):
    response = requests.get(
        url=f"https://api.pexels.com/v1/search?query={city}",
        headers={"Authorization": PEXELS_KEY},
    )

    return response.json()["photos"][0]["src"]["original"]


def get_weather_data(city, state):
    geocoding_params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    geocoding_url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(geocoding_url, params=geocoding_params)

    # print(response.json()) # returns json dict with information about a city

    content = response.json()

    # latitude = content[0]["lat"] # these lines are for printing
    # longitude = content[0]["lon"] # before try except is added

    # print(f"latitude: {latitude}, longitude: {longitude}") # obvious

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    weather_params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(weather_url, params=weather_params)

    # content = response.json()
    # print(content) returns a large amount of weather data about a city

    content = json.loads(response.content)

    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except (KeyError, IndexError):
        return None
